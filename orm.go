package main

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

type loglevel string

const (
	LOGLEVELPROD  loglevel = "PROD"
	LOGLEVELDEBUG loglevel = "DEBUG"
)

var (
	currLogLevel = LOGLEVELPROD
)

func SetLogLevel(level loglevel) {
	currLogLevel = level
}

// Select grabs the data from the target table with the selectors given
// selectors can be of len(selectors) == 0
func Select(target interface{}, selectors ...string) *Scope {
	scope := &Scope{target: target}
	return scope.Select(selectors...)
}

// Where adds a comparisonOperatorConst to the query
func Where(target interface{}, fieldTarget string, conditionType comparisonOperatorConst, condition interface{}) *Scope {
	scope := &Scope{target: target}
	return scope.Where(fieldTarget, conditionType, condition)
}

// Exec generates the query and returns the result of that generated query
func (scope *Scope) Exec() interface{} {
	targetType := reflect.TypeOf(scope.target)

	scope.sql = scope.fields.toSQL()

	scope.sql += " " + fmt.Sprintf("FROM `%s`", strings.ToLower(targetType.Name())+"s")

	scope.sql += " " + scope.wheres.toSQL()

	if scope.limit > 0 {
		scope.sql += " " + fmt.Sprintf("LIMIT %d", scope.limit)
	}

	scope.sql = strings.TrimSpace(scope.sql)

	db, _ := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/blog")

	rows, err := db.Query(scope.sql, scope.queryValues...)
	defer rows.Close()
	if err != nil {
		fullErr := scope.addError(err.Error())
		log.Println(fullErr)
		return nil
	}
	result := scope.scan(rows)
	if scope.limit == 1 {
		return reflect.Indirect(result).Index(0).Interface()
	}
	return reflect.Indirect(result).Interface()
}

func (scope *Scope) scan(rows *sql.Rows) reflect.Value {
	targetType := reflect.TypeOf(scope.target)
	resultSlice := reflect.MakeSlice(reflect.SliceOf(targetType), 0, 0)
	columns, _ := rows.Columns()
	for rows.Next() {
		targets := make([]interface{}, len(columns))
		fillFields := make([]interface{}, len(columns))
		var ignored interface{}
		for index, column := range columns {
			targets[index] = &ignored
			fillFields[index] = nil
			for i := 0; i < targetType.NumField(); i++ {
				field := targetType.Field(i)
				if val, ok := field.Tag.Lookup(tagID); ok {
					if strings.EqualFold(val, column) {
						if field.Type.Kind() == reflect.Ptr {
							targets[index] = reflect.New(field.Type).Addr()
						} else {
							value := reflect.New(reflect.PtrTo(field.Type))
							value.Elem().Set(reflect.New(field.Type).Elem().Addr())
							targets[index] = value.Interface()
							fillFields[index] = field
						}
						break
					}
				}
			}
		}
		err := rows.Scan(targets...)
		if err != nil {
			fullErr := scope.addError(err.Error())
			log.Println(fullErr)
			return resultSlice
		}
		resObj := reflect.New(targetType)
		for i, f := range fillFields {
			if f != nil {
				field := f.(reflect.StructField)
				if val := reflect.ValueOf(targets[i]).Elem().Elem(); val.IsValid() {
					resObj.Elem().FieldByName(field.Name).Set(val)
				}
			}
		}
		resultSlice = reflect.Append(resultSlice, resObj.Elem())
	}
	return resultSlice
}
