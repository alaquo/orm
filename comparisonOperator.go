package main

import "fmt"

type comparisonOperatorConst uint8

const (
	LargerThan comparisonOperatorConst = iota << 4
	LargerOrEqualThan
	SmallerThan
	SmallerOrEqualThan
	Equals
	NotEquals
	Like
	Contains
	In
	IsNil
)

type comparisonOperator struct {
	sql          string
	beforeTarget string
	afterTarget  string
	beforeValue  string
	afterValue   string

	prepareTarget func(interface{}) string
	prepareValue  func(interface{}) string
}

func (c comparisonOperatorConst) Operator() comparisonOperator {
	switch c {
	case LargerOrEqualThan:
		return comparisonOperator{sql: ">= ?"}
	case LargerThan:
		return comparisonOperator{sql: "> ?"}
	case SmallerThan:
		return comparisonOperator{sql: "< ?"}
	case SmallerOrEqualThan:
		return comparisonOperator{sql: "<= ?"}
	case Equals:
		return comparisonOperator{sql: "= ?"}
	case NotEquals:
		return comparisonOperator{sql: "<> ?"}
	case Like:
		return comparisonOperator{sql: "LIKE ?"}
	case Contains:
		return comparisonOperator{
			sql: "LIKE ?",
			prepareValue: func(val interface{}) string {
				return fmt.Sprintf("%%%v%%", val)
			},
		}
	}
	return comparisonOperator{}
}
