package main

import (
	"github.com/go-sql-driver/mysql"
	"log"
)

type test struct {
	ID          int `aq_orm:"blabla"`
	ToiletPaper bool
}

// Liveblog for testing purposes
type Liveblog struct {
	ID          int            `json:"id" aq_orm:"id"`
	URL         string         `json:"url" aq_orm:"url"`
	Title       string         `json:"title" aq_orm:"title"`
	Description string         `json:"description" aq_orm:"description"`
	Status      string         `json:"status" aq_orm:"status"`
	Published   string         `json:"-" aq_orm:"published"`
	PublishedAt mysql.NullTime `json:"publishedAt" aq_orm:"published_at"`
	Deleted     bool           `json:"-" aq_orm:"deleted"`
}

func main() {
	SetLogLevel(LOGLEVELDEBUG)
	newQ := Select(Liveblog{}).Where("Description", Contains, "Day 2").Where("ID", Equals, 2).OrWhere("ID", Equals, 3)
	res := newQ.Exec()
	if res != nil {
		log.Printf("%+v\n", res)
	}

	//Select().Join().Last().Where().Limit().Order()
	log.Printf("%+v\n", newQ.GetSQL())
}
