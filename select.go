package main

import (
	"fmt"
	"log"
	"reflect"
	"strings"
)

type field struct {
	parent     *Scope
	target     *reflect.StructField
	columnName string

	validations interface{}
}

type fields []*field

/** Type field */

// setColumnName sets the value of the columnName field in the field struct
func (field *field) setColumnName() {
	columnName, ok := columnTagForFieldInInterface(field.parent.target, field.target.Name)
	if !ok {
		err := field.parent.addError(fmt.Sprintf("Target: %s - Error %v", field.target.Name, columnName))
		log.Println(err)
		return
	}
	field.columnName = columnName
}

// toSQL generates the individual part for the field of the SELECT part of the SQL statement
// Returns the table name with the column name
// Example return value: `table_name`.`column_name`
func (field *field) toSQL() string {
	tableName := strings.ToLower(reflect.TypeOf(field.parent.target).Name()) + "s"
	if strings.EqualFold(field.columnName, "") {
		field.setColumnName()
	}
	return fmt.Sprintf("`%s`.`%s`", tableName, field.columnName)
}

/** Type fields (field collection) */

// toSQL generates the SELECT part of the SQL Query based on the fields in the fields array.
// Condition len(fields) > 1
// If comparisonOperatorConst not met return: SELECT *
// Returns a SELECT statement of the SQL Query
// Example return value: SELECT `table_name`.`column_name`, `table_name`.`column_name`
func (fields *fields) toSQL() string {
	if len(*fields) < 1 {
		return "SELECT *"
	}
	var fieldSQLs []string
	for _, field := range *fields {
		fieldSQLs = append(fieldSQLs, field.toSQL())
	}
	return "SELECT " + strings.Join(fieldSQLs, ", ")
}
