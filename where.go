package main

import (
	"fmt"
	"log"
	"reflect"
	"strings"
)

type where struct {
	parent             *Scope
	column             *reflect.StructField
	comparisonOperator comparisonOperator
	condition          interface{}
	not                bool
	logicalOperator    string // AND || OR - Default: AND
	orWhere            *where
}

type wheres []*where

/** Type where */

func (where *where) toSQL() string {
	columnName, ok := columnTagForFieldInInterface(where.parent.target, where.column.Name)
	if !ok {
		err := where.parent.addError(fmt.Sprintf("Target: %s - Error %v", where.column.Name, columnName))
		log.Println(err)
		return ""
	}
	tableName := strings.ToLower(reflect.TypeOf(where.parent.target).Name()) + "s"
	if where.comparisonOperator.prepareValue != nil {
		where.parent.queryValues = append(where.parent.queryValues, where.comparisonOperator.prepareValue(where.condition))
	} else {
		where.parent.queryValues = append(where.parent.queryValues, where.condition)
	}
	whereSql := fmt.Sprintf("`%s`.`%s` %s", tableName, columnName, where.comparisonOperator.sql)
	if where.orWhere != nil {
		whereSql = whereSql + " OR " + where.orWhere.toSQL()
		if strings.EqualFold(where.logicalOperator, "AND") {
			whereSql = fmt.Sprintf("(%s)", whereSql)
		}
	}
	return whereSql
}

/** Type wheres ([]*where) */

func (wheres *wheres) toSQL() string {
	if len(*wheres) < 1 {
		return ""
	}
	var whereSQls []string
	for _, where := range *wheres {
		whereSQls = append(whereSQls, where.toSQL())
	}
	return fmt.Sprintf("WHERE %s", strings.Join(whereSQls, " AND "))
}

func (wheres *wheres) getByIndex(index int) *where {
	for i, where := range *wheres {
		if index == i {
			return where
		}
	}
	return nil
}
