package main

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"runtime"
	"strings"
)

type Scoper interface {
	Select(...string) *Scope
	Where(string, comparisonOperatorConst, interface{}) *Scope
	Group(...string) *Scope
	Order(...string) *Scope
	Limit(uint) *Scope
	Asc() *Scope
	Desc() *Scope
	First() *Scope
	Last() *Scope
	Exec() interface{}
}

type Scope struct {
	target    interface{}
	selectors []string
	fields    fields
	wheres    wheres
	errors    []error
	limit     uint

	queryValues []interface{}

	sql string
}

func (scope *Scope) Select(selectors ...string) *Scope {
	for _, selector := range selectors {
		structField, ok := fieldForInterface(scope.target, selector)
		if !ok {
			err := scope.addError(fmt.Sprintf("Target: %s - Field not found: %s", reflect.TypeOf(scope.target).Name(), selector))
			log.Println(err)
			continue
		}
		scope.fields = append(scope.fields, &field{parent: scope, target: &structField})
	}
	return scope
}

func (scope *Scope) Where(column string, conditionType comparisonOperatorConst, condition interface{}) *Scope {
	newWhere := scope.newWhereStruct(column, conditionType, condition, "AND")
	if newWhere == nil {
		return scope
	}
	scope.wheres = append(scope.wheres, newWhere)
	return scope
}

func (scope *Scope) OrWhere(column string, conditionType comparisonOperatorConst, condition interface{}) *Scope {
	orWhere := scope.newWhereStruct(column, conditionType, condition, "OR")
	if orWhere == nil {
		return scope
	}
	lastWhere := scope.wheres.getByIndex(len(scope.wheres) - 1)
	for {
		if lastWhere.orWhere == nil {
			lastWhere.orWhere = orWhere
			break
		}
		lastWhere = lastWhere.orWhere
	}

	return scope
}

func (scope *Scope) Group(columns ...string) *Scope {

	return scope
}

func (scope *Scope) Order(columns ...string) *Scope {

	return scope
}

func (scope *Scope) Limit(limit uint) *Scope {
	scope.limit = limit
	return scope
}

func (scope *Scope) Asc() *Scope {

	return scope
}

func (scope *Scope) Desc() *Scope {

	return scope
}

func (scope *Scope) First() *Scope {

	return scope.Limit(1).Asc()
}

func (scope *Scope) Last() *Scope {

	return scope.Limit(1).Desc()
}

func (scope *Scope) Valid() bool {
	return len(scope.errors) <= 1
}

func (scope *Scope) GetSQL() string {
	return scope.sql
}

func (scope *Scope) addError(errorString string) error {
	logPrefix := "aq_orm"

	pc, _, _, ok := runtime.Caller(1)
	if !ok {
		panic("Error generation went wrong")
	}
	detail := runtime.FuncForPC(pc)

	if currLogLevel == LOGLEVELDEBUG {
		fileName, line := detail.FileLine(pc)
		filePath := strings.Split(fileName, "/")
		logPrefix = fmt.Sprintf("DEBUG | %s:%v", filePath[len(filePath)-1], line)
	}
	methodName := strings.SplitN(detail.Name(), ".", 2)[1]
	fullErr := fmt.Sprintf("%s - %s - %s", logPrefix, methodName, errorString)
	err := errors.New(fullErr)
	scope.errors = append(scope.errors, err)
	return err
}

func (scope *Scope) newWhereStruct(column string, conditionType comparisonOperatorConst, condition interface{}, logical string) *where {
	structField, ok := fieldForInterface(scope.target, column)
	if !ok {
		err := scope.addError(fmt.Sprintf("Target: %s - Field not found: %s", reflect.TypeOf(scope.target).Name(), column))
		log.Println(err)
		return nil
	}
	return &where{
		parent:             scope,
		column:             &structField,
		not:                false,
		comparisonOperator: conditionType.Operator(),
		condition:          condition,
		logicalOperator:    logical,
	}
}
