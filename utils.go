package main

import (
	"reflect"
	"strings"
)

func fieldForInterface(object interface{}, fieldName string) (reflect.StructField, bool) {
	return reflect.TypeOf(object).FieldByName(fieldName)
}

func tagForFieldInInterface(object interface{}, fieldName, tagID string) (string, bool) {
	field, ok := fieldForInterface(object, fieldName)
	if !ok {
		return "FIELD_NOT_FOUND", false
	}
	tagVal, ok := field.Tag.Lookup(tagID)
	if !ok {
		return "TAG_NOT_FOUND", false
	}
	return tagVal, true
}

func columnTagForFieldInInterface(object interface{}, fieldName string) (string, bool) {
	val, ok := tagForFieldInInterface(object, fieldName, tagID)
	if !ok {
		if strings.EqualFold(val, "TAG_NOT_FOUND") {
			val = fieldName
		} else {
			return val, false
		}
	}
	return val, true
}
